/**
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 *
 * In this naive approach, a block is allocated by simply incrementing
 * the brk pointer.  A block is pure payload. There are no headers or
 * footers.  Blocks are never coalesced or reused. Realloc is
 * implemented directly using mm_malloc and mm_free.
 *
 * NOTE TO STUDENTS: Replace this header comment with your own header
 * comment that gives a high level description of your solution.
 *
 * These semantics match the semantics of the corresponding libc malloc,
 * realloc, and free routines.  Type man malloc to the shell for complete
 * documentation.
 */
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your identifying information in the following struct.
 ********************************************************/
team_t team = {
	/* Team name */
	"Hyrule Warriors",
	/* First member's full name */
	"Ryan Kingston",
	/* First member's UID */
	"U0782618",
	/* Second member's full name (leave blank if none) */
	"John McKay",
	/* Second member's UID (leave blank if none) */
	"U0777125"
};

/**
 * TODO
 * (?) we need to make sure the pseudo-header and epilogue don't go off the end of the heap
 * In coalesce, in the case (!prevallocated && next is allocated) we need to update the links, 
 * 	we currently only update the size
 * In coalesce, the first case, check to see if doing nothing is correct.
 * 
 * Update and add comments
 */

/* Function prototypes for internal helper routines */
static void add_link_to_end(uint32_t* bp);
static void *extend_heap(size_t words);  /* uses mem_sbrk to extend the heap */
static void place(void *bp, size_t asize);
static void *find_fit(size_t asize);
static void *coalesce(void *bp);
static void printblock(void *bp); 
static void print_free_list(); 
static void checkheap(int verbose);
static void checkblock(void *bp);

/** single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/** rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (ALIGNMENT)*(((size) + (ALIGNMENT) + ((ALIGNMENT)-1)) / (ALIGNMENT));

/* Basic constants and macros from pg 857 */
#define WSIZE 4             /* Word size in bytes.  This is also the size of the header and footer.*/
#define DSIZE 8             /* Double word size in bytes */
#define CHUNKSIZE (1 << 12) /* Extend heap by this amount */

#define MAX(x, y) ((x) > (y)? (x) : (y))           /* if x is greater than y, give us x, otherwise give us y, so if they are equal to eachother then we get y. */

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc) ((size) | (alloc))       /* information in the header and footer, which is a bit-wise or, which combines the size of the block and whether it is allocated or not */

/* Read and write a word at address p */
#define GET(p)      (*(uint32_t *)(p))         /* dereferences the pointer, which gets the value at that memory location */
#define PUT(p, val) (*(uint32_t *)(p) = (val)) /* dereferences the pointer, and changes the value at that memory location */

/* Read the size and allocated fields from address p */
#define GET_SIZE(p)  (GET(p) & ~0x7)               /* masking the value at address p, to get everything except the last bits, which the last bits are used to tell whether it is allocated */
#define GET_ALLOC(p) (GET(p) & 0x1)                /* masking the value at address p, to get the last bit, which we use to tell whether it was allocated */

/* Given block ptr bp, compute address of its header and footer */
#define HDRP(bp) ((uint32_t *)(bp) - 1)            /* cast the pointer as a char*, which is 1-byte, and then we subtract WSIZE, which is 4-bytes, to get it to point to the beginning/address of the header */
#define FTRP(bp) ((uint32_t *)(bp) + GET_SIZE(HDRP(bp))/WSIZE - 2) /* using the pointer, that points to the start of memory we add the block size, which includes the header,footer, and memory, then we subtract DSIZE to land at the beginning of the footer address */

/* Given block ptr bp, compute address of next and previous blocks */
#define NEXT_BLKP(bp) ((uint32_t *)(bp) + GET_SIZE(((uint32_t *)(bp) - 1))/WSIZE) /* we get the size out of the current header and we move forward that many bytes to point to the memory of the next block */
#define PREV_BLKP(bp) ((uint32_t *)(bp) - GET_SIZE(((uint32_t *)(bp) - 2))/WSIZE) /* we get the size out of the previous footer and then we move back that many bytes from the current memory pointer which leads to the previous memory's address */

/* Pointer to the first payload in the heap */
#define FIRST_PAYLOAD (((uint32_t*) mem_heap_lo())+4)
/* Pointer to the first word of the heap*/
/* HEAP_START is a pseudo-node, 
 * HEAP_START->next points to the first payload
 * HEAP_START->prev points to the last payload
 * HEAP_START isn't an actual payload so it should never be allocated or returned */
#define HEAP_START ((uint32_t*) mem_heap_lo())

typedef struct link {
	struct link* next;
	struct link* prev;
} node;

/* turns on checkheap and function logging */
#define DEBUG
#undef DEBUG

#define PADDING
#undef PADDING

/**
 * mm_init - initialize the malloc package.
 *
 * Before calling mm_malloc, mm_realloc, or mm_free, the application program
 * (i.e. the trace-driven driver program that you will use to evaluate your 
 * implementation) calls mm_init to perform any necessary initializations, such
 * as allocating heap area.
 * @return -1 on error.  0 on success.
 */
int mm_init(void)
{
	/* Extend the heap */
	if(mem_sbrk(4*WSIZE) == (void*)-1)
		return -1;
	PUT(HEAP_START, 0);				 /* HEAP_START next */
	PUT(HEAP_START + 1, PACK(0, 0)); /* HEAP_START prev */
	PUT(HEAP_START + 2, PACK(0, 1)); /* pseudo-footer PREV_BLKP should see this and not go any farther back */
	PUT(HEAP_START + 3, PACK(0, 1)); /* epilogue NEXT_BLKP should see this and not go any farther forward */
	
	uint32_t* bp;
	size_t words = CHUNKSIZE/WSIZE;
	size_t size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
	
	if((bp = mem_sbrk(size)) == (void*)-1)
		return -1;
	
	/* Initialize free block header/footer and the epilogue header */
	/* Replaces the old epilogue with the header to the new memory */
	PUT(HDRP(bp), PACK(size, 0));         /* Free block header */
	PUT(FTRP(bp), PACK(size, 0));         /* Free block footer */
	PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* new epilogue */

	/* Create the first free block of memory */

	((node*)bp)->next = (node*)HEAP_START;
	((node*)bp)->prev = (node*)HEAP_START;

	/* point to the first and the last node */
	((node*)(HEAP_START))->next = (node*)bp;
	((node*)(HEAP_START))->prev = (node*)bp;

	return 0;
}

/**
 * mm_malloc - Allocate a block with at least size bytes of payload 
 * Always allocate a block whose size is a multiple of the alignment.
 *
 * The mm_malloc routine returns a pointer to an allocated block payload of at
 * least size bytes.  The entire allocated block should lie within the heap
 * region and should not overlap with any other allocated chunk.
 *
 * We will be comparing your implementation to the version of malloc supplied
 * in the standard C library (libc). Since the libc malloc always returns
 * payload pointers that are aligned to 8 bytes, your malloc implmentation
 * should do likewise and always return 8-byte aligned pointers.
 */
void *mm_malloc(size_t size) 
{
#ifdef DEBUG
	printf("Before mm_malloc %zu Bytes\n", size);
	checkheap(0);
	printf("a %zu\n", size);
	fflush(stdout);
#endif
	size_t asize;      /* Adjusted block size */
	size_t extendsize; /* Amount to extend heap if no fit */
	uint32_t *bp;

	/* If the heap hasn't been initialized, initialize it */
	if (HEAP_START == 0){
		mm_init();
	}

	/* Ignore spurious requests */
	if (size == 0)
		return NULL;

#ifdef PADDING
	size += 16 + DSIZE;
#endif
	/* Adjust block size to include overhead and alignment reqs. */
	if (size <= DSIZE)
		asize = 2*DSIZE;
	else
		asize = ALIGN(size); /* aligns by ALIGNMENT macro */

	/* Search the free list for a fit */
	bp = find_fit(asize);
	/* If we found a fit, put it there and return the address */
	if(bp != NULL) {
		place(bp, asize);
		return bp;
	}

	/* No fit found. Get more memory and place the block */
	extendsize = MAX(asize,CHUNKSIZE);
	/* Extend the heap */
	bp = extend_heap(extendsize/WSIZE);
	/* If we were unable to extend the heap, return null to let the caller know there was an issue */
	if (bp == NULL)
		return NULL;
	place(bp, asize);
	return bp;
}


/**
 * mm_free - Free a block
 *
 * The mm_free routine frees the block pointed to by ptr.  It returns nothing.
 * This routine is only guaranteed to work when the passed pointer (ptr) was
 * returned by an earlier call to mm_malloc or mm_realloc and has not yet been
 * freed.
 */
void mm_free(void *bp)
{
#ifdef DEBUG
	printf("Before mm_free %p\n", bp);
	checkheap(0);
	printf("mm_free(%p)\n", bp);
	fflush(stdout);	
#endif
	if (bp == 0) 
		return;

	/* If the heap hasn't been initialized initialize it. */
	if (HEAP_START == 0){
		mm_init();
	}

	/* Set the value in header and footer to the same size as it was before, but set it to unallocated. */
	size_t size = GET_SIZE(HDRP(bp));
	PUT(HDRP(bp), PACK(size, 0));
	PUT(FTRP(bp), PACK(size, 0));
	
	/* Insert the new free block at the end of the free list */
	add_link_to_end((uint32_t*) bp);
	/* Coalesce the blocks to possibly make the free blocks bigger */
	coalesce(bp);
}

/**
 * mm_realloc - Naive implementation of realloc
 * 
 * Concise restatement of below:
 * mm_realloc resizes the allocated memory at ptr to be size.  It can move the 
 * memory to make it bigger.  It returns the address of the resized memory.
 * - if ptr is NULL, the call is equivalent to mm_malloc(size);
 * - if size is eqal to zero, the call is equivalent to mm_free (ptr);
 * - if ptr is not NULL, it must point to memory that has been allocated on the heap
 *
 * The mm_realloc routine returns a pointer to an allocated region of at least
 * size bytes with the following constraints.
 * - if ptr is NULL, the call is equivalent to mm_malloc(size);
 * - if size is eqal to zero, the call is equivalent to mm_free (ptr);
 * - if ptr is not NULL, it must have been returned by an earlier call to 
 * mm_malloc or mm_realloc.  The call to mm_realloc changes the size of the 
 * memory block pointed to by ptr (the old block) to size bytes and returns the
 * address of the new block.  Notice that the address of the new block might be
 * the same as the old block, or it might be different, depending on your
 * implementation, the amount of internal fragmentation in the old block, and
 * the size of the realloc request.
 *
 * The contents of the new block are the same as the those of the old ptr block, 
 * up to the minimum of the old and new sizes.  Everything else is unitialized.
 * For example, if the old block is 8 bytes and the new block is 12 bytes, then
 * the first 8 bytes of the new block are identical to the first 8 bytes of the
 * old block and the last 4 bytes are unitialized.  Similarly, if the old block
 * are identical to the first 4 byets of the old block.
 */
void *mm_realloc(void *ptr, size_t requestedSize)
{
	/* If requestedSize == 0 then this is just free, and we return NULL. */
	if(requestedSize == 0) {
		mm_free(ptr);
		return 0;
	}

	/* If oldptr is NULL, then this is just malloc. */
	if(ptr == NULL) {
		return mm_malloc(requestedSize);
	}

	size_t oldsize = GET_SIZE(HDRP(ptr));
	uint32_t next_block_alloc = GET_ALLOC(HDRP(NEXT_BLKP(ptr)));
	size_t size_next_block = GET_SIZE(HDRP(NEXT_BLKP(ptr)));
	size_t combined_size = oldsize + size_next_block;
 
	if(combined_size >= requestedSize && !next_block_alloc){
		
		node* next_block = (node*)NEXT_BLKP(ptr);
		node* bp_prev_free = ((node*)next_block)->prev;  /* pointer to the previous free block, right before bp */
		node* bp_next_free = ((node*)next_block)->next;  /* pointer to the next free block, right after bp */
		node bp_links = *next_block;                     /* store bp's links */	

		if(combined_size - requestedSize > (4*WSIZE))
		{
			/* Place the header and footer for this block */
			PUT(HDRP(ptr), PACK(requestedSize, 1));
			PUT(FTRP(ptr), PACK(requestedSize, 1));
			
			next_block = (node*)NEXT_BLKP(ptr);
			/* place the header and footer for the block we just made by splitting the original block */
			PUT(HDRP(next_block), PACK(combined_size - requestedSize, 0));
			PUT(FTRP(next_block), PACK(combined_size - requestedSize, 0));

			/* Move the links that were in bp, to the new free block we just created */
			/* Set bp's next and prev */
			((node*)next_block)->next = bp_links.next;
			((node*)next_block)->prev = bp_links.prev;
			/* Move the links that were pointing to the old ptr to the new ptr */
			bp_prev_free->next = (node*)next_block;
			bp_next_free->prev = (node*)next_block;
		}
		else
		{
			printf("Only big enough for 1 block.");
			fflush(stdout);
			oldsize += GET_SIZE(HDRP(NEXT_BLKP(ptr))); /* The size of the combined block is the size of this block and the one after it */
			PUT(HDRP(ptr), PACK(oldsize, 1));
			PUT(FTRP(ptr), PACK(oldsize, 1));

			/* Since this block is now allocated, have the next block point to the
			 * previous block and the previous block point to the next block */
			bp_prev_free->next = bp_next_free;
			bp_next_free->prev = bp_prev_free;

			return ptr;
		}
	}

	/* Allocate a block of size size with our mm_malloc function */
	void* newptr = mm_malloc(requestedSize);

	/* If realloc() fails the original block is left untouched  */
	if(!newptr) {
		return 0;
	}

	/* Copy the lesser of oldSize and the size they requested,
	 * if we didn't we would copy past their block of memory */
	int sizeToCopy;
	if(requestedSize < oldsize) 
		sizeToCopy = requestedSize;
	else
		sizeToCopy = oldsize;
	memcpy(newptr, ptr, sizeToCopy);

	/* Free the old block. */
	mm_free(ptr);

	return newptr;
}

/* Places bp at the end of the linked free list 
 * We need to take care of bp's prev and next,
 * HEAP_START's prev, and HEAP_START's->prev next
 */
static void add_link_to_end(uint32_t* bp)
{	
	/* Set bp's previous to point to last free block which is HEAP_START's prev */
	((node*)bp)->prev = ((node*)HEAP_START)->prev;
	/* Set bp's next to point to the HEAP_START, to complete the full circle of the free list */
	((node*)bp)->next = (node*)HEAP_START;
	/* Set the last free block's next to the new block, which is bp*/
	((node*)HEAP_START)->prev->next = (node*) bp;
	/* Set HEAP_START's prev to point to the new block, which is bp */
	((node*)HEAP_START)->prev = (node*) bp;
}

static void *extend_heap(size_t words) 
{
#ifdef DEBUG
	printf("extend_heap %zu words\n", words);
#endif
	uint32_t *bp;
	size_t size;

	/* Allocate an even number of words to maintain alignment */
	size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
	/* increase the heap by size */
	bp = mem_sbrk(size);
	/* if bp == -1, there was an error trying to increase the size of the heap */
	if ((long)(bp) == -1)
		return NULL;

	/* Initialize free block header/footer and the epilogue header */
	PUT(HDRP(bp), PACK(size, 0));         /* Free block header */
	PUT(FTRP(bp), PACK(size, 0));         /* Free block footer */
	PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* New epilogue */

	add_link_to_end(bp);

	/* Coalesce if the previous block was free */
	return coalesce(bp);
}


/**
 * coalesce - Boundary tag coalescing. 
 * @return ptr to coalesced block
 */
static void *coalesce(void *bp) 
{
#ifdef DEBUG
	printf("Before coalesce %p\n", bp);
	checkheap(0);
#endif
	size_t prev_alloc = (GET_SIZE((uint32_t*)bp - 2) == 0) ? 
		1 : GET_ALLOC(FTRP(PREV_BLKP(bp))); /* get whether or not the block before bp is allocated */
	size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp))); /* get whether or not the block after bp is allocated */
	size_t size = GET_SIZE(HDRP(bp)); /* get the size of bp */

	/* If the block before and after bp are allocated there is nothing to coalesce */
	if (prev_alloc && next_alloc)
	{
#ifdef DEBUG
		printf("Coalescing %p: prev and next are allocated\n", bp);
#endif
		return bp;
	}

	/* If the block before is allocated 
	 * but the block after isn't then combine the block after with this block */
	else if (prev_alloc && !next_alloc)
	{
#ifdef DEBUG
		printf("Coalescing: prev is allocated next isn't\n");
#endif

		/* combine this block with the next block */
		node* next_block = (node*)NEXT_BLKP(bp);
		next_block->prev->next = next_block->next;
		next_block->next->prev = next_block->prev;

		/* Update the size in the header and footer */
		size += GET_SIZE(HDRP(NEXT_BLKP(bp))); /* The size of the combined block is the size of this block and the one after it */
		PUT(HDRP(bp), PACK(size, 0));
		PUT(FTRP(bp), PACK(size, 0));
	}
	/* if the block after is allocated but the block before isn't then combine this block with the block before this one */
	/* prev !allocated => has next, prev links
	 * bp   !allocated => has next, prev links
	 * next  allocated
	 * we want to remove bp's free-block links from the free list
	 *  	bp->prev->next = bp->next
	 *  	bp->next->prev = bp->prev
	 * adjust the size of prev to include size of bp */
	else if (!prev_alloc && next_alloc)
	{
#ifdef DEBUG
		printf("Coalescing: prev isn't allocated next is\n");
#endif

		/* Pull this block out of the free list */
		((node*)bp)->prev->next = ((node*)bp)->next;
		((node*)bp)->next->prev = ((node*)bp)->prev;

		size += GET_SIZE(HDRP(PREV_BLKP(bp))); /* this size of the combined block is the size of this block and the one before it */
		/* Update the header of the previous block and the footer of this block.*/
		PUT(FTRP(bp), PACK(size, 0));
		PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));

		bp = PREV_BLKP(bp);
	}

	/* The block before and after are unallocated */
	else
	{
#ifdef DEBUG
		printf("Coalescing: neither prev or next are allocated \n");
#endif

		/* Pull the next block out of the free list */
		node* next_block = (node*)NEXT_BLKP(bp);
		next_block->prev->next = next_block->next;
		next_block->next->prev = next_block->prev;

		/* Pull this block out of the free list */
		((node*)bp)->prev->next = ((node*)bp)->next;
		((node*)bp)->next->prev = ((node*)bp)->prev;

		size += GET_SIZE(HDRP(PREV_BLKP(bp))) + GET_SIZE(FTRP(NEXT_BLKP(bp))); /* the total size is the size of this block, the block before, and the block after */
		/* Update the header of the previous block and the footer of the next block */
		PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
		PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));

		bp = PREV_BLKP(bp);
	}

	return bp;
}


/* 
 * place - Place block of alignedSize at start of free block bp 
 *         and split if remainder would be at least minimum block size
 */
static void place(void *bp, size_t alignedSize)
{   /* We used the code in the book on pg.884 for this method */

	size_t currentSize = GET_SIZE(HDRP(bp)); /* current size of the free block */
	node* bp_prev_free = ((node*)bp)->prev;  /* pointer to the previous free block, right before bp */
	node* bp_next_free = ((node*)bp)->next;  /* pointer to the next free block, right after bp */
	node bp_links = *(node*)bp;              /* store bp's links */ 

	/* Minimum block size is 16 bytes */
	/* If the current block's size is large enought to hold another block, then split the block */
	if ((currentSize - alignedSize) >= (4*WSIZE)) {
		/* Place the header and footer for this block */
		PUT(HDRP(bp), PACK(alignedSize, 1));
		PUT(FTRP(bp), PACK(alignedSize, 1));
		bp = NEXT_BLKP(bp);

		/* place the header and footer for the block we just made by splitting the original block */
		PUT(HDRP(bp), PACK(currentSize-alignedSize, 0));
		PUT(FTRP(bp), PACK(currentSize-alignedSize, 0));

		/* Move the links that were in bp, to the new free block we just created */
		/* Set bp's next and prev */
		((node*)bp)->next = bp_links.next;
		((node*)bp)->prev = bp_links.prev;
		/* Move the links that were pointing to the old bp to the new bp */
		bp_prev_free->next = (node*)bp;
		bp_next_free->prev = (node*)bp;
	}
	/* if the current block is only big enough to hold the requested block, don't split it */
	else {
		PUT(HDRP(bp), PACK(currentSize, 1));
		PUT(FTRP(bp), PACK(currentSize, 1));

		/* Since this block is now allocated, have the next block point to the
		 * previous block and the previous block point to the next block */
		bp_prev_free->next = bp_next_free;
		bp_next_free->prev = bp_prev_free;
	}
}


/* 
 * find_fit - Find a fit for a block with asize bytes 
 */
static void *find_fit(size_t asize)
{
	node* bp = ((node*)HEAP_START);
	/* Go through each free block in the heap. */
	for(bp = bp->next; bp != (node*)HEAP_START; bp = bp->next){
		if(asize <= GET_SIZE(HDRP(bp))){
			return bp;
		}
	}
	return NULL; /* No fit */
}


static void printblock(void *bp) 
{
	size_t headerSize, headerAlloc, footerSize, footerAlloc;

	headerSize = GET_SIZE(HDRP(bp));
	headerAlloc = GET_ALLOC(HDRP(bp));
	footerSize = GET_SIZE(FTRP(bp));
	footerAlloc = GET_ALLOC(FTRP(bp));

	/* Footer size is only zero when we have reached the end of the heap */
	if (headerSize == 0 && headerAlloc) {
		printf("%p: EPILOGUE\n", bp);
		return;
	}

	/* print the address of the header, the size, and whether or not it is allocated */
	printf("%p: header: [%zu Bytes:%c] footer: [%zu Bytes:%c]\n", bp,
		   headerSize, (headerAlloc ? 'a' : 'f'),
		   footerSize, (footerAlloc ? 'a' : 'f'));
}


/**
 * Verifies that the given block is aligned to an 8 byte boundary 
 * and the block header matches the footer
 */
static void checkblock(void *bp)
{
    if ((size_t)bp % 8)
        printf("Error: %p is not doubleword aligned\n", bp);
    if (GET(HDRP(bp)) != GET(FTRP(bp))){
        printf("Error: header does not match footer\n");
		printf("Size header: %d\n", GET_SIZE(HDRP(bp)));
		printf("Size footer: %d\n", GET_SIZE(FTRP(bp)));
    }
}


void print_free_list(){
	printf("Begin Free List\n");
	node* bp = ((node*)HEAP_START);
	int block_num = 0;
	printf("HEAP_START->next: %p\n", ((node*)bp)->next);
	printf("HEAP_START->prev: %p\n", ((node*)bp)->prev);
	/* Go through each free block in the heap. */
	for(bp = bp->next; bp != (node*)HEAP_START; bp = bp->next){
		printf("Free block: %d\n", block_num);
		printblock(bp);
		printf("%p->next %p \n", bp, ((node*)bp)->next);
		printf("%p->prev %p \n", bp, ((node*)bp)->prev);
		block_num++;
	}
	printf("End Free List\n");
}

/* 
 * checkheap - Minimal check of the heap for consistency 
 */
void checkheap(int verbose) 
{
	uint32_t *bp = FIRST_PAYLOAD;

	if (verbose)
		printf("Heap (%p):\n", HEAP_START);
	if(verbose)
		print_free_list();

	/* If the prologue header doesn't have the values that we put there when it was initialized then our prologue is bad */
	/* The size of the prologue header should be DSIZE and it shouldn't be allocated */
	size_t prologue_size = GET_SIZE(HEAP_START + 2);
	uint32_t prologue_allocated = GET_ALLOC(HEAP_START + 2);
	if ((prologue_size != 0)|| !prologue_allocated){
		printf("Bad prologue header\n");
		printf("Prologue size: %zu\n", prologue_size);
		printf("prologue allocated? %d\n", prologue_allocated);
	}

	/* Print and check all the blocks in the heap */
	for (bp = FIRST_PAYLOAD; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
		if (verbose) 
			printblock(bp);
		checkblock(bp);
	}

	/* Print the last block */
	if (verbose)
		printblock(bp);

	/* The size of the HEADER shouldn't be zero and it should not be allocated */
	if ((GET_SIZE(HDRP(bp)) != 0) || !(GET_ALLOC(HDRP(bp))))
		printf("Bad epilogue header\n");
}
